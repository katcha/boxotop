package com.xebia.boxotop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.xebia.R;

import com.googlecode.androidannotations.annotations.EActivity;

@EActivity(R.layout.splash_screen)
public class MainActivity extends Activity {
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Splash screen.
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        Intent movieIntent = new Intent(MainActivity.this, MovieActivity_.class);
                        startActivity(movieIntent);
                        finish();
                    }
                },
                SPLASH_TIME_OUT);

    }
}