package com.xebia.boxotop;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.text.TextUtils;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;

import com.xebia.R;
import com.xebia.boxotop.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EActivity(R.layout.movie)
public class MovieActivity extends Activity implements OnQueryTextListener{

    //Attributs
    @ViewById(R.id.movie_list)
    ListView movies;
    String response = null;
    JSONObject jsonResponse = null;
    String movieTitle = null;
    String movieSynopsis = null;
    String movieCasting = null;
    List<String> mListTitle  = new ArrayList<String>();
    List<String> mListSynopsis  = new ArrayList<String>();
    List<String> mListProfile  = new ArrayList<String>();
    List<String> mListRelease  = new ArrayList<String>();
    List<String> mListScore  = new ArrayList<String>();
    List<String> mListActors  = new ArrayList<String>();
    ArrayList<String> mListSimilar  = new ArrayList<String>();
    ArrayList<String> mListCasting  = new ArrayList<String>();
    HashMap<String, ArrayList<String>> new_values = new HashMap<String, ArrayList<String>>();
    ArrayList<String> id_list = new ArrayList<String>();
    ArrayList<String> actors_list = new ArrayList<String>();

    @AfterViews
    public void CustomizeActionBar(){
        final ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.action_bar_custom_view);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        retrieveMovies();
    }

    //Create the search bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService( this.SEARCH_SERVICE );
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String newText){
        if (TextUtils.isEmpty(newText)){
            movies.clearTextFilter();
        }
        else{
            movies.setFilterText(newText.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // TODO Auto-generated method stub
        return false;
    }

    @Background
    public void retrieveMovies(){

        try{
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            response = restTemplate.getForObject(Constants.XEBIA_server, String.class);
        }

        catch (Exception e){
            Log.e("REST-ERROR", " = " + Log.getStackTraceString(e));
            displayToast("Oups, probléme de connexion!");
        }

        //Build the movies list.
        try{
            jsonResponse = new JSONObject(response);
            JSONArray jsonMovieNode = jsonResponse.optJSONArray(Constants.TAG_ROOT);

            //title + synopsis parsing
            for(int i = 0; i < jsonMovieNode.length(); i++) {
                JSONObject jsonChildNode = jsonMovieNode.getJSONObject(i);
                movieTitle = jsonChildNode.optString(Constants.TAG_TITLE).toString();
                movieSynopsis = jsonChildNode.optString(Constants.TAG_SYNOPSIS).toString();

                //Release date parsing.
                JSONObject release = jsonChildNode.getJSONObject("release_dates");
                String date = release.getString("theater");
                mListRelease.add(date);

                //Rating score parsing.
                JSONObject rating = jsonChildNode.getJSONObject("ratings");
                String score = rating.getString("audience_score");
                mListScore.add(score);

                //Profile IMG parsing.
                JSONObject poster = jsonChildNode.getJSONObject("posters");
                String img = poster.getString("profile");
                mListProfile.add(img);

                //Similar movie parsing.
                JSONObject links = jsonChildNode.getJSONObject("links");
                String similar = links.getString("similar");
                try {
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                    response = restTemplate.getForObject(similar, String.class);
                }
                catch (Exception e) {}
                try {
                    jsonResponse = new JSONObject(response);
                    JSONArray jsonOtherNode = jsonResponse.optJSONArray(Constants.TAG_ROOT);
                    for (int o = 0; o < jsonOtherNode.length(); o++) {
                        JSONObject jsonOtherChild = jsonOtherNode.getJSONObject(o);
                        String movieSimilarTitle = jsonOtherChild.optString(Constants.TAG_TITLE).toString();
                        mListSimilar.add(movieSimilarTitle);
                    }
                }
                catch (Exception e){}

                //Add the lists.
                id_list.add("" + jsonChildNode.optString("id").toString());
                mListTitle.add(movieTitle);
                mListSynopsis.add(movieSynopsis);
                actors_list = new ArrayList<String>();

                //Actors parsing.
                JSONArray jsonCastArray = jsonChildNode.optJSONArray("abridged_cast");
                for(int j = 0; j < jsonCastArray.length(); j++){
                    try{
                        JSONObject jsonCast = jsonCastArray.getJSONObject(j);
                        movieCasting = jsonCast.optString("name").toString();
                        actors_list.add(""+jsonCast.optString("name").toString());
                        new_values.put(""+jsonChildNode.optString("id").toString(),actors_list);
                        mListCasting.add(movieCasting);
                    }
                    catch (Exception e){}
                }
            }
        }
        catch (Exception e) {
            Log.e("LIST-ERROR", " = " + Log.getStackTraceString(e));
            displayToast("Impossible de récupérer le box office!");
        }

        for (Map.Entry<String,ArrayList<String>> entry : new_values.entrySet()) {
            //Log.e("Id",""+entry.getKey());
            //Log.e("actors_list",""+entry.getValue());

            mListActors.add(entry.getValue().toString());

        }

        AddMovieList();
    }

    @UiThread
    public void AddMovieList(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mListTitle);
        movies.setAdapter(adapter);
        movies.setTextFilterEnabled(true);
        movies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(MovieActivity.this, DetailActivity_.class);
                Bundle bundle = new Bundle();
                bundle.putString("title", mListTitle.get(position));
                bundle.putString("synopsis", mListSynopsis.get(position));
                bundle.putString("profile", mListProfile.get(position));
                bundle.putString("release", mListRelease.get(position));
                bundle.putString("score", mListScore.get(position));
                bundle.putString("casting", mListActors.get(position));
                bundle.putString("similar", mListSimilar.get(position));
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });
    }

    @UiThread
    public void displayToast(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}