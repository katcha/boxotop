package com.xebia.boxotop;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;
import com.xebia.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

@EActivity(R.layout.detail)
public class DetailActivity extends Activity {

    @ViewById(R.id.txt_movie_title)
    TextView movieTitle;
    @ViewById(R.id.synopsis_content)
    TextView movieSynopsis;
    @ViewById(R.id.casting_content)
    TextView movieCasting;
    @ViewById(R.id.similar_content)
    TextView movieSimilar;
    @ViewById(R.id.release_content)
    TextView movieRelease;
    @ViewById(R.id.score_content)
    TextView movieScore;
    @ViewById(R.id.img_movie_thumbnail)
    ImageView moviePoster;
    Bitmap bm;

    @AfterViews
    @Background
    public void CustomizeActionBar(){
        final ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.action_bar_custom_view);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        String title = getIntent().getExtras().get("title").toString();
        String synopsis = getIntent().getExtras().get("synopsis").toString();
        String casting = getIntent().getExtras().get("casting").toString();
        String profile = getIntent().getExtras().get("profile").toString();
        String similar = getIntent().getExtras().get("similar").toString();
        String release = getIntent().getExtras().get("release").toString();
        String score = getIntent().getExtras().get("score").toString();

        if (title.matches("")){
            movieTitle.setText("Non disponible");
        }
        else{
            movieTitle.setText(title);
        }

        if (synopsis.matches("")){
            movieSynopsis.setText("Non disponible");
        }
        else{
            movieSynopsis.setText(synopsis);
        }

        if (casting.matches("")){
            movieCasting.setText("Non disponible");
        }
        else{
            movieCasting.setText(casting);
        }

        if (casting.matches("")){
            movieSimilar.setText("Non disponible");
        }
        else{
            movieSimilar.setText(similar);
        }

        if (casting.matches("")){
            movieRelease.setText("Non disponible");
        }
        else{
            movieRelease.setText(release);
        }

        if (casting.matches("")){
            movieScore.setText("Non disponible");
        }
        else{
            score = score + '%';
            movieScore.setText(score);
        }

        try {
            Log.e("PROFILE", " = " + profile);
            URL url = new URL(profile.toString());
            URLConnection conn = url.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            moviePoster.setImageBitmap(bm);
            bis.close();
            is.close();
        }
        catch (IOException e) {
            Log.e("BITMAP-ERROR", "Error getting bitmap", e);
        }
    }
}