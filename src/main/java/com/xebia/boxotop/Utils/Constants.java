package com.xebia.boxotop.Utils;

public interface Constants {

    static final String XEBIA_server = "http://xebiamobiletest.herokuapp.com/api/public/v1.0/lists/movies/box_office.json";
    static final String TAG_ROOT = "movies";
    static final String TAG_TITLE = "title";
    static final String TAG_SYNOPSIS = "synopsis";
}
